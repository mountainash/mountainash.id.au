# mountainash.id.au

![mountainash.id.au](mountainash.png)

## URLs

- <https://mountainash.id.au/>

## Stack

- Domain: TPP
- DNS: Cloudflare
- Server: ASO
- CDN: Cloudflare
- Email: Cloudflare to Gmail
- SCM: GitLab
- CI/CD: GitLab
- Analytics: Aptabase via Zaraz (_see below_)

## Development

 ```sh
 python3 -m http.server 3000
 ```

## Analytics Setup

Aptabase offer a [cloud Saas](https://aptabase.com) analytics service, while making the [source code available](https://github.com/aptabase/aptabase). It is free for up to 20,000 events per month.

They don't offer a website JS snippet, so I revere-engineered their NPM module to make a POST request directly to the endpoint.

Using Cloudfare Zaraz we can make a POST request to a Worker to reformat the POST request to be accepted by Aptabase. The Worker is running at <https://aptabase.worker.farm> _under the TRUTH DOMAINS Cloudflare account_ and stored in this repo as [`worker.ts`](./worker.ts). It's important to pass the request payload to the Worker.

![Cloudfare Zaraz](zaraz-settings.png)